package com.example.backend.health.rest;

import com.example.backend.consts.RestConsts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@RequestMapping(RestConsts.REST_HEALTH_API)
public class RestHealthController {

    @Value("${app.version}")
    private String appVersion;

    @GetMapping("/hello-world")
    public String helloWorld() {
        return "Hello World!";
    }

    @GetMapping("/random-number")
    public int getRandomNumber() {
        Random rand = new Random();
        int n = rand.nextInt(1000) + 1;// Obtain a number between [1 - 1000].
        return n;
    }

    @GetMapping("/app-version")
    public String getAppVersion() {
        return appVersion;
    }
}
