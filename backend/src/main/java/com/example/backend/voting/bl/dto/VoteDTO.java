package com.example.backend.voting.bl.dto;

public class VoteDTO {
    private Long id;
    private Integer yesCount;
    private Integer noCount;
    private String name;

    public VoteDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getYesCount() {
        return yesCount;
    }

    public void setYesCount(Integer yesCount) {
        this.yesCount = yesCount;
    }

    public Integer getNoCount() {
        return noCount;
    }

    public void setNoCount(Integer noCount) {
        this.noCount = noCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "VoteDTO{" +
                "id=" + id +
                ", yesCount=" + yesCount +
                ", noCount=" + noCount +
                ", name='" + name + '\'' +
                '}';
    }
}
