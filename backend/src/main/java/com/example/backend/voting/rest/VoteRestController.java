package com.example.backend.voting.rest;

import com.example.backend.consts.RestConsts;
import com.example.backend.voting.bl.VoteService;
import com.example.backend.voting.bl.dto.VoteDTO;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(RestConsts.REST_VOTE_API)
public class VoteRestController {

    private final VoteService voteService;

    public VoteRestController(VoteService voteService) {
        this.voteService = voteService;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public VoteDTO save(@RequestBody final VoteDTO dto) {
        return voteService.save(dto);
    }

    @GetMapping("/find-by-id/{id}")
    public VoteDTO findById(@PathVariable final Long id){
        return voteService.findById(id);
    }

    @GetMapping("/find-all")
    public List<VoteDTO> findAll() {
        return voteService.findAll();
    }

    @DeleteMapping("/delete-by-id/{id}")
    public void deleteById(@PathVariable Long id) {
        voteService.deleteById(id);
    }

    @DeleteMapping("/delete")
    public void delete(@RequestBody final VoteDTO dto) {
        voteService.delete(dto);
    }
}
