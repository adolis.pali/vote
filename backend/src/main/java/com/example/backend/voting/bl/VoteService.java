package com.example.backend.voting.bl;

import com.example.backend.voting.bl.dto.VoteDTO;

import java.util.List;

public interface VoteService {

    VoteDTO save(VoteDTO dto);

    VoteDTO findById(Long id);
    List<VoteDTO> findAll();

    void deleteById(Long id);
    void delete(VoteDTO dto);
}
