package com.example.backend.voting.bl.mapper;

import com.example.backend.voting.bl.dto.VoteDTO;
import com.example.backend.voting.model.VoteEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class DTOEntityMapper {

    private final ModelMapper modelMapper;

    public DTOEntityMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public VoteDTO convertToDTO(VoteEntity entity) {
        VoteDTO voteDTO = modelMapper.map(entity, VoteDTO.class);
        return voteDTO;
    }

    public VoteEntity convertToEntity(VoteDTO dto) {
        VoteEntity voteEntity = modelMapper.map(dto, VoteEntity.class);
        return voteEntity;
    }
}
