package com.example.backend.voting.model.repo;

import com.example.backend.voting.model.VoteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VoteRepo extends CrudRepository<VoteEntity, Long> {

    List<VoteEntity> findAll();
}
