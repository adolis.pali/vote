package com.example.backend.voting.bl;

import com.example.backend.voting.bl.dto.VoteDTO;
import com.example.backend.voting.bl.mapper.DTOEntityMapper;
import com.example.backend.voting.model.VoteEntity;
import com.example.backend.voting.model.repo.VoteRepo;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
class VoteServiceImpl implements VoteService {

    private final VoteRepo voteRepo;
    private final DTOEntityMapper dtoEntityMapper;

    VoteServiceImpl(VoteRepo voteRepo, DTOEntityMapper dtoEntityMapper) {
        this.voteRepo = voteRepo;

        this.dtoEntityMapper = dtoEntityMapper;
    }

    @Override
    public VoteDTO save(VoteDTO dto) {
       VoteEntity entity = dtoEntityMapper.convertToEntity(dto);
       VoteEntity savedEntity = voteRepo.save(entity);
       VoteDTO savedDTO = dtoEntityMapper.convertToDTO(savedEntity);

       return savedDTO;
    }

    @Override
    public VoteDTO findById(Long id) {
        VoteEntity foundEntity = voteRepo.findById(id).get();
        VoteDTO foundDTO = dtoEntityMapper.convertToDTO(foundEntity);
        return foundDTO;
    }

    @Override
    public List<VoteDTO> findAll() {
        List<VoteDTO> foundDTOList = voteRepo.findAll().stream()
                                             .map(e -> dtoEntityMapper.convertToDTO(e)).collect(Collectors.toList());
        return foundDTOList;
    }

    @Override
    public void deleteById(Long id) {
        voteRepo.deleteById(id);
    }

    @Override
    public void delete(VoteDTO dto) {
        VoteEntity entity = dtoEntityMapper.convertToEntity(dto);
        voteRepo.delete(entity);
    }
}
