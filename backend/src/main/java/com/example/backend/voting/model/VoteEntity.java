package com.example.backend.voting.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VOTE")
public class VoteEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "YES_COUNT")
    private Integer yesCount;

    @Column(name = "NO_COUNT")
    private Integer noCount;

    @Column(name = "NAME")
    private String name;

    public VoteEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getYesCount() {
        return yesCount;
    }

    public void setYesCount(Integer yesCount) {
        this.yesCount = yesCount;
    }

    public Integer getNoCount() {
        return noCount;
    }

    public void setNoCount(Integer noCount) {
        this.noCount = noCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "VotingEntity{" +
                "id=" + id +
                ", yesCount=" + yesCount +
                ", noCount=" + noCount +
                ", name='" + name + '\'' +
                '}';
    }
}
