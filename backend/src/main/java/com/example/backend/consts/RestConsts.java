package com.example.backend.consts;

public interface RestConsts {
    String REST_VOTE_API = "api/vote";
    String REST_HEALTH_API = "api/health";
    String REST_ENV_API = "api/env";
}
