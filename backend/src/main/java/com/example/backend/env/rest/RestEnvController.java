package com.example.backend.env.rest;

import com.example.backend.consts.RestConsts;
import com.example.backend.env.bl.EnvService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(RestConsts.REST_ENV_API)
public class RestEnvController {

    private final EnvService envService;

    public RestEnvController(EnvService envService) {
        this.envService = envService;
    }

    @GetMapping("/variables")
    public String helloWorld() {
        return envService.getAllEnvVariables();
    }
}
