package com.example.backend.env.bl;

import org.springframework.stereotype.Service;

import java.util.Map;

@Service
class EnvServiceImpl implements EnvService {

    @Override
    public String getAllEnvVariables() {
        String envVariables = "";
        Map<String, String> env = System.getenv();
        for (String envName : env.keySet()) {
            envVariables += String.format("%s=%s,", envName, env.get(envName));
        }
        return envVariables;
    }
}
